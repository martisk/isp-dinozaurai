<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface $paginator
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Movie::class);

        $this->paginator = $paginator;
    }

    public function getMovieListPaginated(int $page = 1, int $limit = 20, $criteria = [])
    {
        $qb = $this->createQueryBuilder('r');

        foreach ($criteria as $cr) {
            switch ($cr['type']) {
                case 'like':
                    if (!isset($cr['value'])) {
                        break;
                    }
                    $qb = $qb->andWhere("r." . $cr['property'] . " LIKE '" . $cr['value'] . "'");
                    break;

                case 'range':
                    if (!isset($cr['value1']) || !isset($cr['value2'])) {
                        break;
                    }
                    $qb = $qb->andWhere("r." . $cr['property'] . " BETWEEN '" . $cr['value1'] . "' AND '" . $cr['value2'] . "'");
                    break;
            }
        }

        return $this->paginator->paginate($qb, $page, $limit);
    }
}
