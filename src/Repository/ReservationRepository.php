<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Reservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface $paginator
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Reservation::class);

        $this->paginator = $paginator;
    }

    public function getClientReservationListPaginated(
        Client $client,
        int $page = 1,
        string $movie = '',
        string $dateFrom = '',
        string $dateTo = '',
        string $status = '',
        int $limit = 20
    ) {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->andWhere('r.client = :client')
            ->setParameter('client', $client->getId());

        if ($movie) {
            $qb
                ->innerJoin('r.movie', 'm', 'WITH', 'r.movie = m.id')
                ->andWhere('m.title LIKE :movie')
                ->setParameter('movie', '%' . $movie . '%');
        }

        if ($dateFrom) {
            $qb
                ->andWhere('r.date >= :dateFrom')
                ->setParameter('dateFrom', $dateFrom);
        }

        if ($dateTo) {
            $qb
                ->andWhere('r.date <= :dateTo')
                ->setParameter('dateTo', $dateTo);
        }

        if ($status) {
            $qb
                ->andWhere('r.status LIKE :status')
                ->setParameter('status', '%'. $status .'%');
        }

        $qb->orderBy('r.id', 'ASC');

        return $this->paginator->paginate($qb, $page, $limit);
    }

    public function getReservationListPaginated(int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('r');

        return $this->paginator->paginate($qb, $page, $limit);
    }

    public function getLatestAvailableMovieDate(string $id)
    {
        $qb = $this->createQueryBuilder('r')
            ->select('r.date', 'r.days')
            ->where('r.movie = :movie')
            ->setParameter('movie', $id)
            ->orderBy('r.date', 'ASC');

        $results = $qb->getQuery()->getResult();

        $minDate = date_add(
            new \DateTime(),
            date_interval_create_from_date_string(
                '100 years'
            )
        );

        foreach ($results as $result) {
            $calcDate = date_add(
                $result['date'],
                date_interval_create_from_date_string(
                    $result['days'] . ' days'
                )
            );

            if ($minDate > $calcDate) {
                $minDate = $calcDate;
            }
        }

        return $minDate->format('Y-m-d');
    }
}
