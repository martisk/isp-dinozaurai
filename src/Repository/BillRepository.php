<?php

namespace App\Repository;

use App\Entity\Bill;
use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Bill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bill[]    findAll()
 * @method Bill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Bill::class);

        $this->paginator = $paginator;
    }

    public function getClientBillListPaginated(Client $client, int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('b')
            ->where('b.client = :client')
            ->setParameter('client', $client->getId());

        return $this->paginator->paginate($qb, $page, $limit);
    }

    public function getBillListPaginated(int $page = 1, int $limit = 20)
    {
        $qb = $this->createQueryBuilder('b');

        return $this->paginator->paginate($qb, $page, $limit);
    }
}
