<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191104233416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, child_comment_id INT DEFAULT NULL, movie_id INT DEFAULT NULL, user_id INT DEFAULT NULL, date DATETIME NOT NULL, rating INT NOT NULL, content VARCHAR(255) NOT NULL, level INT NOT NULL, INDEX IDX_9474526C1CB68378 (child_comment_id), INDEX IDX_9474526C8F93B6FC (movie_id), INDEX IDX_9474526CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE store (id INT AUTO_INCREMENT NOT NULL, city VARCHAR(255) NOT NULL, street_name VARCHAR(255) NOT NULL, building_number VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, working_hours_from DATETIME NOT NULL, working_hours_to DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suggestion (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, date DATETIME NOT NULL, content VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_DD80F31B19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE password_recovery (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, valid_until DATETIME NOT NULL, used TINYINT(1) NOT NULL, INDEX IDX_63D40109A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, bill_id INT DEFAULT NULL, client_id INT DEFAULT NULL, date DATETIME NOT NULL, days INT NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_42C849551A8C12F5 (bill_id), INDEX IDX_42C8495519EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bill (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, date DATETIME NOT NULL, status VARCHAR(255) NOT NULL, amount NUMERIC(10, 2) NOT NULL, INDEX IDX_7A2119E319EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, store_id INT DEFAULT NULL, user_id INT DEFAULT NULL, personal_code VARCHAR(11) NOT NULL, works_from DATETIME NOT NULL, worked_to DATETIME DEFAULT NULL, admin TINYINT(1) NOT NULL, address VARCHAR(255) NOT NULL, INDEX IDX_5D9F75A1B092A811 (store_id), UNIQUE INDEX UNIQ_5D9F75A1A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, imdb NUMERIC(10, 1) NOT NULL, genres LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', director VARCHAR(255) NOT NULL, actors LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', rent_price NUMERIC(5, 2) NOT NULL, amount INT NOT NULL, users_rating NUMERIC(5, 1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_reservation (movie_id INT NOT NULL, reservation_id INT NOT NULL, INDEX IDX_6D4EC5A28F93B6FC (movie_id), INDEX IDX_6D4EC5A2B83297E7 (reservation_id), PRIMARY KEY(movie_id, reservation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_store (movie_id INT NOT NULL, store_id INT NOT NULL, INDEX IDX_811542EB8F93B6FC (movie_id), INDEX IDX_811542EBB092A811 (store_id), PRIMARY KEY(movie_id, store_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, register_date DATETIME NOT NULL, UNIQUE INDEX UNIQ_C7440455A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C1CB68378 FOREIGN KEY (child_comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE suggestion ADD CONSTRAINT FK_DD80F31B19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE password_recovery ADD CONSTRAINT FK_63D40109A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849551A8C12F5 FOREIGN KEY (bill_id) REFERENCES bill (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495519EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE bill ADD CONSTRAINT FK_7A2119E319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1B092A811 FOREIGN KEY (store_id) REFERENCES store (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE movie_reservation ADD CONSTRAINT FK_6D4EC5A28F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_reservation ADD CONSTRAINT FK_6D4EC5A2B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_store ADD CONSTRAINT FK_811542EB8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_store ADD CONSTRAINT FK_811542EBB092A811 FOREIGN KEY (store_id) REFERENCES store (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD disabled TINYINT(1) NOT NULL, ADD restrictions INT NOT NULL, ADD fullname VARCHAR(255) NOT NULL, ADD phone_number VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C1CB68378');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1B092A811');
        $this->addSql('ALTER TABLE movie_store DROP FOREIGN KEY FK_811542EBB092A811');
        $this->addSql('ALTER TABLE movie_reservation DROP FOREIGN KEY FK_6D4EC5A2B83297E7');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849551A8C12F5');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C8F93B6FC');
        $this->addSql('ALTER TABLE movie_reservation DROP FOREIGN KEY FK_6D4EC5A28F93B6FC');
        $this->addSql('ALTER TABLE movie_store DROP FOREIGN KEY FK_811542EB8F93B6FC');
        $this->addSql('ALTER TABLE suggestion DROP FOREIGN KEY FK_DD80F31B19EB6921');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C8495519EB6921');
        $this->addSql('ALTER TABLE bill DROP FOREIGN KEY FK_7A2119E319EB6921');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE store');
        $this->addSql('DROP TABLE suggestion');
        $this->addSql('DROP TABLE password_recovery');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE bill');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_reservation');
        $this->addSql('DROP TABLE movie_store');
        $this->addSql('DROP TABLE client');
        $this->addSql('ALTER TABLE user DROP disabled, DROP restrictions, DROP fullname, DROP phone_number');
    }
}
