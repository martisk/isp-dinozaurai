<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191117131404 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C1CB68378');
        $this->addSql('DROP INDEX IDX_9474526C1CB68378 ON comment');
        $this->addSql('ALTER TABLE comment CHANGE rating rating INT DEFAULT NULL, CHANGE child_comment_id parent_comment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CBF2AF943 FOREIGN KEY (parent_comment_id) REFERENCES comment (id)');
        $this->addSql('CREATE INDEX IDX_9474526CBF2AF943 ON comment (parent_comment_id)');
        $this->addSql('ALTER TABLE employee MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE employee DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE employee DROP id');
        $this->addSql('ALTER TABLE employee ADD PRIMARY KEY (personal_code)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CBF2AF943');
        $this->addSql('DROP INDEX IDX_9474526CBF2AF943 ON comment');
        $this->addSql('ALTER TABLE comment CHANGE rating rating INT NOT NULL, CHANGE parent_comment_id child_comment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C1CB68378 FOREIGN KEY (child_comment_id) REFERENCES comment (id)');
        $this->addSql('CREATE INDEX IDX_9474526C1CB68378 ON comment (child_comment_id)');
        $this->addSql('ALTER TABLE employee DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE employee ADD id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE employee ADD PRIMARY KEY (id)');
    }
}
