<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite savo vardą ir pavardę...',
                ],
                'label' => 'Vardas, pavardė'
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite el. pašto adresą...',
                ],
                'label' => 'El. pašto adresas',
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'Telefono numeris',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite telefono numerį',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
