<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Slaptažodžiai turi sutapti',
                'required' => true,
                'first_options' => [
                    'label' => 'Naujas slaptažodis',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Įrašykite slaptažodį...',
                    ]
                ],
                'second_options' => [
                    'label' => 'Pakartoti slaptažodį',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Pakartokite slaptažodį...',
                    ]
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
