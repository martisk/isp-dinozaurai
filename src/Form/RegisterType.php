<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite savo vardą ir pavardę...',
                ],
                'label' => 'Vardas, pavardė'
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite el. pašto adresą...',
                ],
                'label' => 'El. pašto adresas',
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'mapped' => 'false',
                'invalid_message' => 'Slaptažodžiai turi sutapti',
                'first_options' => [
                    'label' => 'Slaptažodis',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Įrašykite slaptažodį...',
                    ]
                ],
                'second_options' => [
                    'label' => 'Pakartoti slaptažodį',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Pakartokite slaptažodį...',
                    ]
                ]
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'Telefono numeris',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Įrašykite telefono numerį',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
