<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=1)
     */
    private $imdb;

    /**
     * @ORM\Column(type="array")
     */
    private $genres = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $director;

    /**
     * @ORM\Column(type="array")
     */
    private $actors = [];

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $rentPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private $usersRating;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Store", inversedBy="movies")
     */
    private $stores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="movie")
     */
    private $comments;

    public function __construct()
    {
        $this->stores = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getImdb(): ?string
    {
        return $this->imdb;
    }

    public function setImdb(string $imdb): self
    {
        $this->imdb = $imdb;

        return $this;
    }

    public function getGenre(): ?array
    {
        return $this->genres;
    }

    public function setGenre(array $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    public function getDirector(): ?string
    {
        return $this->director;
    }

    public function setDirector(string $director): self
    {
        $this->director = $director;

        return $this;
    }

    public function getActors(): ?array
    {
        return $this->actors;
    }

    public function setActors(array $actors): self
    {
        $this->actors = $actors;

        return $this;
    }

    public function getRentPrice(): ?string
    {
        return $this->rentPrice;
    }

    public function setRentPrice(string $rentPrice): self
    {
        $this->rentPrice = $rentPrice;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getUsersRating(): ?string
    {
        return $this->usersRating;
    }

    public function setUsersRating(string $usersRating): self
    {
        $this->usersRating = $usersRating;

        return $this;
    }

    /**
     * @return Collection|Store[]
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }

    public function addStore(Store $store): self
    {
        if (!$this->stores->contains($store)) {
            $this->stores[] = $store;
        }

        return $this;
    }

    public function removeStore(Store $store): self
    {
        if ($this->stores->contains($store)) {
            $this->stores->removeElement($store);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setMovie($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getMovie() === $this) {
                $comment->setMovie(null);
            }
        }

        return $this;
    }
}
