<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=11)
     */
    private $personalCode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $worksFrom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $workedTo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store", inversedBy="employees")
     */
    private $store;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="employee", cascade={"persist", "remove"})
     */
    private $user;

    public function getPersonalCode(): string
    {
        return $this->personalCode;
    }

    public function setPersonalCode(string $personalCode): self
    {
        $this->personalCode = $personalCode;

        return $this;
    }

    public function getWorksFrom(): ?\DateTimeInterface
    {
        return $this->worksFrom;
    }

    public function setWorksFrom(\DateTimeInterface $worksFrom): self
    {
        $this->worksFrom = $worksFrom;

        return $this;
    }

    public function getWorkedTo(): ?\DateTimeInterface
    {
        return $this->workedTo;
    }

    public function setWorkedTo(?\DateTimeInterface $workedTo): self
    {
        $this->workedTo = $workedTo;

        return $this;
    }

    public function isAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
