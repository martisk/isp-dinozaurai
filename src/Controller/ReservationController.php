<?php

namespace App\Controller;

use App\Entity\Bill;
use App\Entity\Movie;
use App\Entity\Reservation;
use App\Entity\User;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservation")
 */
class ReservationController extends AbstractController
{
    /**
     * @Route("/cancel/{id}", name="app_reservation_cancel")
     */
    public function cancelReservation(Request $request, $id)
    {
        $reservation = $this->getDoctrine()->getRepository(Reservation::class)->find($id);

        if ($reservation) {
            $reservation->setStatus('atšaukta');
            $bill = $reservation->getBill();

            $bill->setStatus('atšaukta');

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('cancel-success', 'Rezervacija atšaukta sėkmingai.');
        }

        return $this->redirectToRoute('app_profile_reservations');
    }

    /**
     * @Route("/submit", name="app_reservation_submit")
     * @throws \Exception
     */
    public function makeReservation(Request $request, MailerService $mailerService)
    {
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find(
            $request->request->get('movieId')
        );
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'fullname' => $request->request->get('user'),
        ]);
        $startDate = new \DateTime(
            $request->request->get('startDate')
        );
        $endDate = new \DateTime(
            $request->request->get('endDate')
        );
        $dateDiff = date_diff($endDate, $startDate);

        $client = $user->getAccount();

        $reservation = new Reservation();
        $reservation
            ->setDays($dateDiff->days)
            ->setDate($startDate)
            ->setStatus('laukiama')
            ->setClient($client)
            ->setMovie($movie);

        $bill = new Bill();
        $bill
            ->setClient($client)
            ->setStatus('neapmokėta')
            ->setDate(new \DateTime())
            ->setAmount($movie->getRentPrice())
            ->addReservation($reservation);

        $client
            ->addBill($bill)
            ->addReservation($reservation);

        $em = $this->getDoctrine()->getManager();
        $em->persist($reservation);
        $em->persist($bill);
        $em->persist($client);
        $em->flush();

        $mailerService->sendMail(
            'Rezervacijos atlikimas',
            $user->getEmail(),
            'emails/reservationCreated.html.twig',
            [
                'fullName' => $user->getFullname(),
                'movieTitle' => $movie->getTitle(),
                'date' => $reservation->getDate(),
                'billAmount' => $bill->getAmount(),
            ]
        );

        return new JsonResponse([
            'status' => '201',
        ]);
    }

    /**
     * @Route("/{id}", name="app_reservation_show")
     */
    public function showReservation(Request $request, $id)
    {
        $reservation = $this->getDoctrine()->getRepository(Reservation::class)->find($id);

        if (!$reservation || $reservation->getClient() !== $this->getUser()->getAccount()) {
            $this->addFlash('reservation-not-found', 'Įvyko klaida! Rezervacija, kurios bandėte ieškoti, neegzistuoja.');

            return $this->redirectToRoute('app_profile_reservations');
        }

        return $this->render('reservation/item.html.twig', [
            'reservation' => $reservation,
        ]);
    }
}
