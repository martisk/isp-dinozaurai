<?php

namespace App\Controller;

use App\Entity\PasswordRecovery;
use App\Entity\User;
use App\Form\UserEmailType;
use App\Form\PasswordResetType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/password-recovery")
 */
class PasswordRecoveryController extends AbstractController
{
    /**
     * @Route("/", name="app_password_recovery")
     */
    public function index(Request $request, MailerService $mailerService)
    {
        $form = $this->createForm(UserEmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $email = $form->get('email')->getData();

            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);

            if ($user != null) {
                $passRec = new PasswordRecovery();
                $passRec
                    ->generateRecoveryToken()
                    ->setValidUntil(new \DateTime('now +1 day'))
                    ->setUsed(false)
                    ->setUser($user);

                $em = $this->getDoctrine()->getManager();
                $em->persist($passRec);
                $em->flush();

                $this->addFlash('success', 'Slaptažodžio pakeitimo nuoroda buvo išsiųsta į ' . $email);

                $generatedUrl = $this->generateUrl(
                    'app_recovery_change',
                    [
                        'token' => $passRec->getToken(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $mailerService->sendMail(
                    'Slaptažodžio atkūrimas',
                    $user->getEmail(),
                    'emails/resetPassword.html.twig',
                    [
                        'fullname' => $user->getFullname(),
                        'generatedUrl' => $generatedUrl
                    ]
                );
            } else {
                $emailError = new FormError("El. paštas nerastas");
                $form->get('email')->addError($emailError);
            }
        }

        return $this->render('passwordRecovery/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{token}", name="app_recovery_change")
     */
    public function recoveryChange(
        $token = "",
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $passRec = $this->getDoctrine()->getRepository(PasswordRecovery::class)->findOneBy([
            'token' => $token
        ]);

        $user = null;

        $form = $this->createForm(PasswordResetType::class);
        $form->handleRequest($request);

        if ($passRec != null && !$passRec->getUsed()) {
            $now = new \DateTime();
            $validUntil = $passRec->getValidUntil();
            $interval = $now->diff($validUntil);

            if (!$interval->invert) {
                $user = $passRec->getUser();

                if ($form->isSubmitted() && $form->isValid()) {
                    $user->setPassword(
                        $passwordEncoder->encodePassword(
                            $user,
                            $form->get('password')->getData()
                        )
                    );

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($passRec);
                    $em->flush();
                    $this->addFlash('success', 'Slaptažodis pakeistas sėkmingai, prašome prisijungti');

                    return $this->redirectToRoute('app_login');
                }
            }
        }

        if ($user == null) {
            $this->addFlash('danger', 'Ši nuoruoda neegzistuoja arba pasibaigė jos galiojimo laikas');
            return $this->redirectToRoute('app_home');
        }

        return $this->render('passwordRecovery/changePassword.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
