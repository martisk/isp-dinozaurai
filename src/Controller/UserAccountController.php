<?php

namespace App\Controller;

use App\Entity\Bill;
use App\Entity\Reservation;
use App\Entity\User;
use App\Form\ProfileEditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile")
 */
class UserAccountController extends AbstractController
{
    /**
     * @Route("/", name="app_profile")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(ProfileEditType::class, $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'email' => $form->get('email')->getData()
            ]);

            if ($user != $this->getUser()) {
                $this->addFlash('danger', 'Vartotojas tokiu el. pašto adresu egzistuoja.');
            } else {
                $user = $this->getUser();
                $user = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $this->addFlash('success', 'Profilis atnaujintas sėkmingai.');
            }
        }

        return $this->render('userAccount/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reservations", name="app_profile_reservations")
     */
    public function showReservations(Request $request, $pageLimit)
    {
        $user = $this->getUser();

        $status = $request->query->get('status', '');
        $movie = $request->query->get('movie', '');
        $dateFrom = $request->query->get('date-from', '');
        $dateTo = $request->query->get('date-to', '');

        $pagination = $this->getDoctrine()->getRepository(Reservation::class)->getClientReservationListPaginated(
            $user->getAccount(),
            $request->query->getInt('page', 1),
            $movie,
            $dateFrom,
            $dateTo,
            $status,
            $pageLimit
        );

        return $this->render('userAccount/reservation.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
            'status' => $status,
            'movie' => $movie,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
        ]);
    }

    /**
     * @Route("/bills", name="app_profile_bills")
     */
    public function showBills(Request $request, $pageLimit)
    {
        $user = $this->getUser();

        $pagination = $this->getDoctrine()->getRepository(Bill::class)->getClientBillListPaginated(
            $user->getAccount(),
            $request->query->getInt('page', 1),
            $pageLimit
        );

        return $this->render('userAccount/bill.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }
}
