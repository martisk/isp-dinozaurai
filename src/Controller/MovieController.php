<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/rate/{id}", name="app_movie_rate")
     */
    public function rateMovie(Request $request, $id)
    {
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);

        if ($movie) {
            $rating = $request->request->get('rating');

            if ($rating < 1) {
                $rating = 1;
            } else if ($rating > 5) {
                $rating = 5;
            }

            if ($movie->getUsersRating() != null) {
                $rating = ($rating + $movie->getUsersRating()) / 2;
            }

            $movie->setUsersRating($rating);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return JsonResponse::fromJsonString('{ "success": true, "rating": ' . $movie->getUsersRating() . ' }');
        }

        return JsonResponse::fromJsonString('{ "success": false }');
    }

    /**
     * @Route("/comment/{id}", name="app_movie_comment")
     */
    public function commentMovie(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);
        $text = $request->request->get('text');

        if ($movie && $text) {
            $comment = new Comment();
            $comment
                ->setDate(new \DateTime())
                ->setContent($text)
                ->setMovie($movie)
                ->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Komentaras pridėtas sėkmingai.');
        }

        return $this->redirectToRoute('app_movie', [
            'id' => $id
        ]);
    }

        /**
     * @Route("/comment/{id}/reply/{commentID}", name="app_movie_comment_reply")
     */
    public function replyMovieComment(Request $request, $id, $commentID)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $parent = $this->getDoctrine()->getRepository(Comment::class)->find($commentID);
        $text = $request->request->get('text');

        if ($parent && $text) {
            $comment = new Comment();
            $comment
                ->setDate(new \DateTime())
                ->setContent($text)
                ->setParentComment($parent)
                ->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash('success', 'Į komentarą atsakyta sėkmingai.');
        }

        return $this->redirectToRoute('app_movie', [
            'id' => $id
        ]);
    }
}
