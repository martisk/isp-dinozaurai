<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request, $pageLimit)
    {
        $title = $request->query->get('search');
        foreach (explode(' ', $title) as $word) {
            if (empty($word)) {
                continue;
            }
            $criteria[] = [
                'property' => 'title',
                'value' => '%' . $word . '%',
                'type' => 'like'
            ];
        }
        $criteria[] = [
            'property' => 'genres',
            'value' => '%' . $request->query->get('genre') . '%',
            'type' => 'like'
        ];
        $criteria[] = [
            'property' => 'rentPrice',
            'value1' => $request->query->get('min-price'),
            'value2' => $request->query->get('max-price'),
            'type' => 'range'
        ];

        $pagination = $this->getDoctrine()->getRepository(Movie::class)->getMovieListPaginated(
            $request->query->getInt('page', 1),
            $pageLimit,
            $criteria
        );

        return $this->render('home/index.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }

    /**
     * @Route("/movie/{id}", name="app_movie")
     */
    public function showMovie(Request $request, $id)
    {
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);
        $dateWhenAvailable = null;

        if ($movie->getAmount() < 1) {
            $dateWhenAvailable = 'Atsiprašome, tačiau šiuo metu laisvų šio filmo kopijų neturime. 
                Artimiausia data, nuo kada bus galima rezervuoti šį filmą, yra ' .
                $this->getDoctrine()->getRepository(Reservation::class)->getLatestAvailableMovieDate($id)
            ;
        }

        return $this->render('movie/index.html.twig', [
            'id' => $id,
            'dateWhenAvailable' => $dateWhenAvailable,
            'movie' => $movie,
        ]);
    }
}
