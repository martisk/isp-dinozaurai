<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\User;
use App\Form\RegisterType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout() { }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        MailerService $mailerService
    ) {
        $form = $this->createForm(RegisterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'email' => $form->get('email')->getData()
            ]);

            if ($user) {
                $this->addFlash('error', 'Vartotojas tokiu el. pašto adresu egzistuoja.');
            } else {
                $user = new User();
                $user = $form->getData();
                $user
                    ->setPassword(
                        $passwordEncoder->encodePassword(
                            $user,
                            $form->get('password')->getData()
                        )
                    )
                    ->setRoles(['ROLE_USER'])
                    ->setDisabled(false)
                    ->setRestrictions(0);

                $client = new Client();
                $client
                    ->setRegisterDate(new \DateTime())
                    ->setUser($user);

                $user->setClient($client);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->persist($client);
                $em->flush();

                $this->addFlash('success', 'Registracija sėkminga! Nurodytu el. paštu išsiųstas paskyros patvirtinimo laiškas.');

                $generatedUrl = $this->generateUrl(
                    'app_registration_confirm',
                    [
                        'token' => password_hash(
                            $user->getEmail() . $user->getPassword(),
                            PASSWORD_DEFAULT
                        ),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                $mailerService->sendMail(
                    'Registracijos patvirtinimas',
                    $user->getEmail(),
                    'emails/confirmRegistration.html.twig',
                    [
                        'generatedUrl' => $generatedUrl
                    ]
                );

                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration-confirm", name="app_registration_confirm")
     */
    public function registrationConfirm(Request $request)
    {
        $this->addFlash('success', 'Paskyra sėkmingai patvirtinta. Galite prisijungti.');

        return $this->redirectToRoute('app_login');
    }
}
