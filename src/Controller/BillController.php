<?php

namespace App\Controller;

use App\Entity\Bill;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bill")
 */
class BillController extends AbstractController
{
    /**
     * @Route("/{id}", name="app_bill_show")
     */
    public function showBill(Request $request, $id)
    {
        $bill = $this->getDoctrine()->getRepository(Bill::class)->find($id);

        if (!$bill || $bill->getClient() !== $this->getUser()->getAccount()) {
            $this->addFlash('bill-not-found', 'Įvyko klaida! Sąskaita, kurios ieškote, neegzistuoja.');

            return $this->redirectToRoute('app_profile_bills');
        }

        return $this->render('bill/item.html.twig', [
            'bill' => $bill,
        ]);
    }
}
