<?php

namespace App\Controller\BackOffice;

use App\Entity\Bill;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/bill")
 */
class BillController extends AbstractController
{
    /**
     * @Route("/status/{id}", name="admin_bill_status")
     */
    public function changeBillStatus($id)
    {
        $bill = $this->getDoctrine()->getRepository(Bill::class)->find($id);

        if ($bill) {
            $bill->setStatus('apmokėta');

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Sąskaitos statusas sėkmingai pakeistas.');
        }

        return $this->redirectToRoute('admin_bills');
    }
}
