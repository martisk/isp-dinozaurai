<?php

namespace App\Controller\BackOffice;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="admin_user_delete")
     */
    public function deleteUser($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->addFlash('success', 'Vartotojas ištrintas sėkmingai.');
        }

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/toggle/{id}/admin", name="admin_user_toggle_admin")
     */
    public function toggleUserAdmin($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user) {
            $account = $user->getAccount();
            if (method_exists($account, 'setAdmin')) {
                $account->setAdmin(!$account->isAdmin());

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $this->addFlash('success', 'Darbuotojo administratoriaus teisės nustatytos sėkmingai.');
            } else {
                $this->addFlash('danger', 'vartotojas nėra darbuotojas.');
            }
        }

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/change/{id}/restrictions/{val}", name="admin_user_change_restrictions")
     */
    public function changeUserRestrictions($id, $val)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if ($user) {
            $user->setRestrictions($val);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Vartotojo apribojimai nustatyti sėkmingai.');
        }

        return $this->redirectToRoute('admin_users');
    }
}
