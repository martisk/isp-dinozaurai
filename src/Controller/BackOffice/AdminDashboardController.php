<?php

namespace App\Controller\BackOffice;

use App\Entity\Bill;
use App\Entity\Movie;
use App\Entity\Reservation;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminDashboardController extends AbstractController
{
    /**
     * @Route("/", name="app_admin")
     */
    public function index()
    {
        return $this->render('backOffice/adminDashboard/index.html.twig');
    }

    /**
     * @Route("/movies", name="admin_movies")
     */
    public function showMovies(Request $request, $pageLimit)
    {
        $pagination = $this->getDoctrine()->getRepository(Movie::class)->getMovieListPaginated(
            $request->query->getInt('page', 1),
            $pageLimit
        );

        return $this->render('backOffice/adminDashboard/movies/movies.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }

    /**
     * @Route("/users", name="admin_users")
     */
    public function showUsers(Request $request, $pageLimit)
    {
        $pagination = $this->getDoctrine()->getRepository(User::class)->getUserListPaginated(
            $request->query->getInt('page', 1),
            $pageLimit
        );

        return $this->render('backOffice/adminDashboard/users.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }

    /**
     * @Route("/reservations", name="admin_reservations")
     */
    public function showReservations(Request $request, $pageLimit)
    {
        $pagination = $this->getDoctrine()->getRepository(Reservation::class)->getReservationListPaginated(
            $request->query->getInt('page', 1),
            $pageLimit
        );

        return $this->render('backOffice/adminDashboard/reservations.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }

    /**
     * @Route("/bills", name="admin_bills")
     */
    public function showBills(Request $request, $pageLimit)
    {
        $pagination = $this->getDoctrine()->getRepository(Bill::class)->getBillListPaginated(
            $request->query->getInt('page', 1),
            $pageLimit
        );

        return $this->render('backOffice/adminDashboard/bills.html.twig', [
            'pagination' => $pagination,
            'pageLimit' => $pageLimit,
        ]);
    }
}
