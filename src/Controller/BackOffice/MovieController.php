<?php

namespace App\Controller\BackOffice;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitTextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/admin/movies")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/new",name="new_movie")
     * Method({"GET","POST"})
     */
    public function new(Request $request){
        $movie = new Movie();
        $form = $this->createFormBuilder($movie)
        ->add('title', TextType::class, array('attr' => array('class' => 'form-control'),'label' => 'Pavadinimas'))
        ->add('director', TextType::class, array('attr' => array('class' => 'form-control'),'label' => 'Režisierius'))
        ->add('genres', TextType::class, array('attr' => array('class' => 'form-control'),'mapped'=>false,'label' => 'Žanrai'))
        ->add('actors', TextType::class, array('attr' => array('class' => 'form-control'),'mapped'=>false,'label' => 'Aktoriai'))
        ->add('imdb', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Imdb'))
        ->add('rentPrice', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Nuomos kaina'))
        ->add('amount', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Kiekis'))
        ->add('usersRating', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Įvertinimas'))
        ->add('save', SubmitType::class, array(
          'label' => 'Sukurti',
          'attr' => array('class' => 'btn btn-primary mt-3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $movie = $form->getData();
            $movie->setGenre(explode(',',$form->get('genres')->getData()));
            $movie->setActors(explode(',',$form->get('actors')->getData()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($movie);
            $em->flush();
            return $this->redirectToRoute('admin_movies');
        }
        return $this->render('backOffice/adminDashboard/movies/new.html.twig',array(
            'form' => $form->createView()
        ));
    }
    /**
     * @Route("/edit/{id}",name="edit_movie")
     * Method({"GET","POST"})
     */
    public function edit(Request $request, $id){
        $movie = new Movie();
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);
        $form = $this->createFormBuilder($movie)
        ->add('title', TextType::class, array('attr' => array('class' => 'form-control'),'label' => 'Pavadinimas'))
        ->add('director', TextType::class, array('attr' => array('class' => 'form-control'),'label' => 'Režisierius'))
        ->add('genres', TextType::class, array('attr' => array('class' => 'form-control'),'mapped'=>false,'label' => 'Žanrai'))
        ->add('actors', TextType::class, array('attr' => array('class' => 'form-control'),'mapped'=>false,'label' => 'Aktoriai'))
        ->add('imdb', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Imdb'))
        ->add('rentPrice', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Nuomos kaina'))
        ->add('amount', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Kiekis'))
        ->add('usersRating', NumberType::class, array('attr' => array('class' => 'form-control'),'label' => 'Įvertinimas'))
        ->add('save', SubmitType::class, array(
          'label' => 'Keisti',
          'attr' => array('class' => 'btn btn-primary mt-3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $movie->setGenre(explode(',',$form->get('genres')->getData()));
            $movie->setActors(explode(',',$form->get('actors')->getData()));
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('admin_movies');
        }
        
        return $this->render('backOffice/adminDashboard/movies/edit.html.twig',array(
            'form' => $form->createView()
        ));
    }
    /**
     * @Route("/delete/{id}", name="admin_movie_delete")
     */
    public function deleteMovie($id)
    {
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);

        if ($movie) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($movie);
            $em->flush();
            $this->addFlash('success', 'Filmas ištrintas sėkmingai.');
        }

        return $this->redirectToRoute('admin_movies');
    }
    /**
     * @Route("/reset/{id}", name="admin_movie_reset")
     */
    public function resetMovie($id)
    {
        $movie = $this->getDoctrine()->getRepository(Movie::class)->find($id);

        if ($movie) {
            $movie->setUsersRating("0");
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->redirectToRoute('admin_movies');
    }
    /**
     * @Route("/search",name="search_movie")
     * Method({"GET","POST"})
     */
    public function search(Request $request){
        $form = $this->createFormBuilder(null)
        ->add('search', TextType::class, array('attr' => array('class' => 'form-control'),'label' => 'Filmo paieška'))
        ->add('save', SubmitType::class, array(
          'label' => 'Ieškoti',
          'attr' => array('class' => 'btn btn-primary mt-3')
        ))
        ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $search = $form->getData();
            $movies = $this->getDoctrine()->getRepository(Movie::class)->findBy(array('name' => $search));
            return $this->render('backOffice/adminDashboard/movies/search.html.twig',array('movies' => $movies));
        }
        return $this->render('backOffice/adminDashboard/movies/searchBar.html.twig',array(
            'form' => $form->createView()
        ));
    }
}
