<?php

namespace App\Controller\BackOffice;

use App\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/reservation")
 */
class ReservationController extends AbstractController
{
    /**
     * @Route("/cancel/{id}", name="admin_reservation_cancel")
     */
    public function cancelReservation(Request $request, $id)
    {
        $reservation = $this->getDoctrine()->getRepository(Reservation::class)->find($id);

        if ($reservation) {
            $reservation->setStatus('atšaukta');
            $bill = $reservation->getBill()->setStatus('atšaukta');

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Rezervacija atšaukta sėkmingai.');
        }

        return $this->redirectToRoute('admin_reservations');
    }

    /**
     * @Route("/status/{id}", name="admin_reservation_status")
     */
    public function changeReservationStatus(Request $request, $id)
    {
        $reservation = $this->getDoctrine()->getRepository(Reservation::class)->find($id);

        if ($reservation) {
            $reservation->setStatus(
                $reservation->getStatus() == 'laukiama' ? 'paimta' : 'grąžinta'
            );
            $reservation->getBill()->setStatus('apmokėta');

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', 'Rezervacijos statusas pakeistas sėkmingai.');
        }

        return $this->redirectToRoute('admin_reservations');
    }
}
