<?php

namespace App\DataFixtures;

use App\Entity\Bill;
use App\Entity\Client;
use App\Entity\Employee;
use App\Entity\Reservation;
use App\Entity\User;
use App\Entity\Movie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setEmail('admin@dinozaurai.lt')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($this->encoder->encodePassword($user, 'testas123'))
            ->setDisabled(false)
            ->setRestrictions(0)
            ->setFullname('Amber F Ingram')
            ->setPhoneNumber('070 2973 4362');

        $employee = new Employee();
        $employee
            ->setPersonalCode('50110012233')
            ->setWorksFrom(new \DateTime())
            ->setAdmin(true)
            ->setAddress('54 Fraserburgh Rd');

        $user->setEmployee($employee);

        $manager->persist($user);
        $manager->persist($employee);

        $user = new User();
        $user
            ->setEmail('test@test')
            ->setRoles(['ROLE_USER'])
            ->setPassword(
                $this->encoder->encodePassword($user, 'testas123')
            )
            ->setDisabled(false)
            ->setRestrictions(0)
            ->setFullname('Jack R McCarthy')
            ->setPhoneNumber('078 3749 3553');

        $client = new Client();
        $client
            ->setRegisterDate(new \DateTime())
            ->setUser($user);

        $manager->persist($user);
        $manager->persist($client);

        for ($i = 0; $i < 10; $i++) {
            $movie = new Movie();

            $movie
                ->setAmount(5)
                ->setActors([
                    'Jim Carrey', 'Alex Monaco', 'Taylor Momsen'
                ])
                ->setDirector('Jeff Daniels')
                ->setGenre([
                    'Comedy'
                ])
                ->setImdb('8.9')
                ->setRentPrice('5.5')
                ->setTitle('Send air support')
                ->setUsersRating('4.2');

            $bill = new Bill();
            $bill
                ->setDate(new \DateTime())
                ->setStatus('neapmokėta')
                ->setAmount('12.36')
                ->setClient($client);

            $reservation = new Reservation();

            $reservation
                ->setDate(new \DateTime())
                ->setDays(2)
                ->setStatus('laukiama')
                ->setClient($client)
                ->setBill($bill)
                ->setMovie($movie);

            $manager->persist($bill);
            $manager->persist($reservation);
            $manager->persist($movie);
        }

        $manager->flush();
    }
}
