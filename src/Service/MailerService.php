<?php

namespace App\Service;

class MailerService
{
    /**
     * @var \Swift_Mailer $mailer
     */
    private $mailer;

    /**
     * @var string $senderEmail
     */
    private $senderEmail;

    /**
     * @var \Twig\Environment $templating
     */
    private $templating;

    public function __construct(
        \Swift_Mailer $mailer,
        string $emailAddress,
        \Twig\Environment $templating
    ) {
        $this->mailer = $mailer;
        $this->senderEmail = $emailAddress;
        $this->templating = $templating;
    }

    public function sendMail(
        string $messageTitle,
        string $receiverEmail,
        string $template,
        array $variables = []
    ) {
        $messageToSend = (new \Swift_Message($messageTitle))
            ->setFrom($this->senderEmail)
            ->setTo($receiverEmail)
            ->setBody(
                $this->templating->render(
                    $template,
                    $variables
                ),
                'text/html'
            );

        return $this->mailer->send($messageToSend);
    }
}
